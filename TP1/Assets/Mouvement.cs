using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public Rigidbody rb;
    public int color=0;
    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update(){
        
        
        if (Input.GetKey(KeyCode.DownArrow))
        {
            //sprint
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.forward * 0.05f);
            }
            else{
                transform.Translate(Vector3.forward * 0.01f);
            }
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //sprint
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.back * 0.05f);
            }
            else{
                transform.Translate(Vector3.back * 0.01f);
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -2);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 2);
        }
        //saut
        if(Input.GetKeyDown(KeyCode.Space)){
            rb.AddForce(Vector3.up*3f, ForceMode.Impulse);
            //changement de couleur
            if(color==0){
                rb.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.2f, 0.8f, 1f);
                color=1;
            }
            else{
                rb.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, 1f);
                color=0;
            }
        }

    }
}
