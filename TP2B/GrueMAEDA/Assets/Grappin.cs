using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //si on presse espace
        if (Input.GetKey(KeyCode.Space))
        {
        //on casse le joint
        Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }
    //en cas de collision, on accroche les objets avec un joint
    void OnCollisionEnter(Collision Collision)
    {
    if (Collision.gameObject.GetComponent<ArticulationBody>() != null){
    FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();joint.connectedArticulationBody = Collision.articulationBody;}
    }
}
