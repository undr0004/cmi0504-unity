using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiedCommande : MonoBehaviour
{
    public GameObject Pied;


    void Update()
    {
        float input = Input.GetAxis("Pied");
        EtatPied rotationEtat = MoveStateForInput(input);
        PiedControleur controller = Pied.GetComponent<PiedControleur>();
        controller.rotationEtat = rotationEtat;

        
    }

    EtatPied MoveStateForInput(float input)
    {
        if (input > 0)
        {
            return EtatPied.Positif;
        }
        else if (input < 0)
        {
            return EtatPied.Negatif;
        }
        else
        {
            return EtatPied.Fixe;
        }
    }
}
