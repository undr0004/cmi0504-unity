using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glue : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        // Vérifie si la collision se fait avec un ArticulationBody et si il a un tag "Pied" (pour ne pas tout accrocher)
        if (collision.gameObject.GetComponent<ArticulationBody>() != null && collision.gameObject.CompareTag("Pied"))
        {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = collision.articulationBody;
        }
    }

    void Update()
    {
        //Si U est préssée
        if (Input.GetKey(KeyCode.U))
        {
            //on casse le joint entre les pieds et le sol
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }
}