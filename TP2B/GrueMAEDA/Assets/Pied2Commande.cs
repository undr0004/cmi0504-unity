using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pied2Commande : MonoBehaviour
{
    public GameObject Pied2;


    void Update()
    {
        float input = Input.GetAxis("Pied2");
        EtatPied2 rotationEtat = MoveStateForInput(input);
        Pied2Controleur controller = Pied2.GetComponent<Pied2Controleur>();
        controller.rotationEtat = rotationEtat;

        
    }

    EtatPied2 MoveStateForInput(float input)
    {
        if (input > 0)
        {
            return EtatPied2.Positif;
        }
        else if (input < 0)
        {
            return EtatPied2.Negatif;
        }
        else
        {
            return EtatPied2.Fixe;
        }
    }
}
