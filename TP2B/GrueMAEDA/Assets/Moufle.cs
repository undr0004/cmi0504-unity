using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moufle : MonoBehaviour
{

    public float forceMoufle = 500f;
    public ArticulationBody moufle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //commande pour la moufle
        if (Input.GetKey(KeyCode.LeftShift))
        {
        moufle.AddRelativeForce(transform.up * forceMoufle);}
        if (Input.GetKey(KeyCode.LeftControl))
        {
        moufle.AddRelativeForce(transform.up * -forceMoufle);}
    }
}
