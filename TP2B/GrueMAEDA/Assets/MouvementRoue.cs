using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvementRoue : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            // Sprint
            if (Input.GetKey(KeyCode.LeftShift))
            {
                transform.Translate(Vector3.up * 0.05f);
            }
            else
            {
                transform.Translate(Vector3.up * 0.01f);
            }
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            // Sprint
            if (Input.GetKey(KeyCode.LeftShift))
            {
                transform.Translate(Vector3.down * 0.05f);
            }
            else
            {
                transform.Translate(Vector3.down * 0.01f);
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward, 1);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.forward, -1);
        }
    }
}
