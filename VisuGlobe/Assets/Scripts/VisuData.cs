
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisuData : MonoBehaviour
{
    public Transform Cible;
    public GameObject VisuCube;
    public float multiplier;

     Vector3 latloncart(float lat, float lon)
    {
        Vector3 pos;
        float x = 0.5f * Mathf.Cos(lon) * Mathf.Cos(lat);
        float y = 0.5f * Mathf.Cos(lon) * Mathf.Sin(lat);
        float z = 0.5f * Mathf.Sin(lon);
        pos.x = 0.5f * Mathf.Cos((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        pos.y = 0.5f * Mathf.Sin(lat * Mathf.Deg2Rad);
        pos.z = 0.5f * Mathf.Sin((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        return pos;
    }

    // Fonction pour déterminer la couleur basée sur la population
    Color GetPopulationColor(int population)
    {
        float halfMaxPopulation = 19500000; //Moitié de la population max

        if (population <= halfMaxPopulation)
        {
            // Interpoler entre blanc et jaune
            float t = Mathf.InverseLerp(0, halfMaxPopulation, population);
            return Color.Lerp(Color.white, Color.yellow, t);
        }
        else
        {
            // Interpoler entre jaune et rouge
            float t = Mathf.InverseLerp(halfMaxPopulation, 39105000, population); //Population max à 39105000
            return Color.Lerp(Color.yellow, Color.red, t);
        }
    }

    public void VisualCube(float lat, float lon, float val, float multiplier)
    {
        GameObject cube = GameObject.Instantiate(VisuCube);
        Vector3 pos = latloncart(lat, lon);
        cube.transform.position = pos;
        cube.transform.LookAt(Cible, Vector3.back);
        Vector3 echelle = cube.transform.localScale;
        echelle.z = val * multiplier;
        cube.transform.localScale = echelle;
        
        // Changer la couleur du cube basée sur la population
        Renderer rend = cube.transform.Find("Cube").GetComponent<Renderer>();
        rend.material.color = GetPopulationColor((int)val);
    }
}
