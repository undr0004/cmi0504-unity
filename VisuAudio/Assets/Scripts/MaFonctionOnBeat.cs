using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaFonctionOnBeat : MonoBehaviour
{
    // Déclaration d'un délégué pour l'événement onBeat
    public delegate void onBeatEvent();
    public onBeatEvent onBeat;

    private Renderer rend;
    private Color originalColor;

    void Start()
    {
        onBeat += Beat; // Ajoute la méthode Beat au délégué onBeatEvent
        rend = GetComponent<Renderer>(); // Récupère le composant Renderer de l'objet
        if (rend != null)
        {
            originalColor = rend.material.color;  // Stocke la couleur originale au démarrage
        }
    }

    void Update()
    {
        onBeat?.Invoke(); // Appelle la méthode associée à l'événement onBeat, s'il y en a 
    }
    // Méthode déclenchée lors d'un beat
    void Beat()
    {
        if (rend != null)
        {
            StartCoroutine(ChangeColorTemporarily()); // Démarre la coroutine pour changer temporairement la couleur
        }
    }

    IEnumerator ChangeColorTemporarily()
    {
        rend.material.color = Color.magenta;  // Change la couleur en violet
        yield return new WaitForSeconds(0.5f);  // Pour une durée d'une demi-seconde
        rend.material.color = originalColor;  // Retour à sa couleur originale
    }
}
