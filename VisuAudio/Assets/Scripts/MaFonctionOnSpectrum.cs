using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaFonctionOnSpectrum : MonoBehaviour
{
    // Déclaration d'un délégué pour l'événement onSpectrum
    public delegate void onSpectrumEvent(float[] spectrumData);
    public onSpectrumEvent onSpectrum;

    private Renderer rend;
    private AudioSource audioSource; // Déclaration d'une variable pour stocker l'AudioSource

    void Start()
    {
        onSpectrum += SpectrumReaction; // Ajout de la méthode SpectrumReaction au délégué onSpectrumEvent
        rend = GetComponent<Renderer>();
        rend.material.color = Color.green; // Initialisation de la couleur du matériau à vert pour mieux visualiser le cube concerné
        // Trouver l'AudioSource de Audiovis
        audioSource = GameObject.Find("Audiovis").GetComponent<AudioSource>();
        if (audioSource == null)
        {
            Debug.LogError("Pas d'AudioSource trouvé avec le nom spécifié!");
        }
        
    }

    void Update()
    {
        if (audioSource != null)
        {
            // Création d'un tableau de flottants pour contenir les données du spectre
            float[] spectrum = new float[256];  // Par exemple, pour 256 bandes de fréquences
            audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris); // Obtention des données du spectre de fréquences

            // Appelle l'événement onSpectrum avec les données du spectre
            onSpectrum?.Invoke(spectrum);
        }
    }

    void SpectrumReaction(float[] spectrumData)
    {
        // Récupération de la valeur des hautes fréquences
        float highFreqValue = spectrumData[200]; 

        // Vérification si la valeur de fréquence est supérieure à 0.5f
        if (highFreqValue > 0.5f)
        {
            //Mise à jour de la taille de la transformation
            transform.localScale = new Vector3(1, highFreqValue * 5, 1);
        }
        else
        {
            //Sinon taille de la transformation à 1
            transform.localScale = Vector3.one;
        }
    }
}