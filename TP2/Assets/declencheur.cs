using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    // Une variable publique qui stocke le nombre d'objets dans la zone.
    public int compteur = 0; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // La méthode OnTriggerEnter est appelée lorsque le collider de la zone entre en contact avec un autre collider.
    private void OnTriggerEnter(Collider other)
    {
        // On incrémente le compteur de 1 à chaque déclenchement (à chaque nouvel objet dans la zone)
        compteur=compteur+1;
        //On affiche le nombre d'objets présents dans dans la console Unity.
        Debug.Log("Declenché, "+compteur+ " objet(s) dans la zone");

    }
}
