using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCam : MonoBehaviour
{

    //déclaration des deux caméras
    public Camera camCote;
    public Camera camFace;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            // Vérifie quelle caméra est active actuellement et bascule vers l'autre avec la propriété enabled
            if (camCote.enabled)
            {
                camCote.enabled = false;
                camFace.enabled = true;
            }
            else
            {
                camCote.enabled = true;
                camFace.enabled = false;
            }
        }
    }
}
